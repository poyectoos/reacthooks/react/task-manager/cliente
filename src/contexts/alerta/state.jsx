import React, { useReducer } from 'react';

import Context from './context';
import Reducer from './reducer';


import { MA, OA } from '../../types';

const State = props => {
  const initial = {
    alerta: null
  }

  const [ state, dispatch ] = useReducer(Reducer, initial);

  const { alerta } = state;

  const handleShowAlert = (mensaje, categoria) => {
    dispatch({
      type: MA,
      payload: {
        mensaje,
        categoria
      }
    });

    setTimeout(() => {
      dispatch({
        type: OA
      });
    }, 3000);
  }

  return (
    <Context.Provider
      value={
        {
          alerta,
          
          handleShowAlert
        }
      }
    >
      { props.children }
    </Context.Provider>
  );
}

export default State;