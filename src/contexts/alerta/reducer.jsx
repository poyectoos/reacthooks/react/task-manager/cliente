import { MA, OA } from '../../types';

const Reducer = (state, action) => {
  switch (action.type) {
    case MA:
      return {
        alerta: action.payload
      }
    case OA:
      return {
        alerta: null
      }
    
    default:
      return state;
  }
}

export default Reducer;