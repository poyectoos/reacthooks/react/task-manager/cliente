import React, { useReducer } from 'react';

import Context from './context';
import Reducer from './reducer';

import ClienteAxios from '../../config/axios';
import TokenAuth from '../../config/token';

import { RE, RF, OU, LE, LF, CS } from '../../types';

const State = props => {
  const initial = {
    token: localStorage.getItem('token'),
    autenticado: null,
    usuario: null,
    mensaje: null,
    cargando: true
  }

  const [ state, dispatch ] = useReducer(Reducer, initial);

  const { token, autenticado, usuario, mensaje, cargando } = state;

  const addUser = async (data) => {
    const { nombre, email, password } = data;
    try {
      const respuesta = await ClienteAxios.post('/api/usuarios', { nombre, email, password });

      if (respuesta.status === 200) {
        dispatch({
          type: RE,
          payload: respuesta.data
        });

        getUserAuth();
      } else {
        console.error('Respuesta exitosa no esperada');
      }
      
    } catch (error) {
      const alerta = {
        msg: error.response.data.msg,
        categoria: 'error'
      }
      dispatch({
        type: RF,
        payload: alerta
      });
    }
  }

  const getUserAuth = async () => {
    const token = localStorage.getItem('token');

    if (token) {
      TokenAuth(token)
    }

    try {
      const respuesta = await ClienteAxios.get('/api/auth');

      if (respuesta.status === 200) {
        dispatch({
          type: OU,
          payload: respuesta.data
        });
      } else {
        console.log('respuesta exitosa no esperada');
      }

    } catch (error) {
      dispatch({
        type: LF
      });
    }
  }

  // Funcion para loguear alv
  const logIn = async (data) => {
    try {
      const { email, password } = data;

      const respuesta = await ClienteAxios.post('/api/auth', { email, password });
      
      if (respuesta.status === 200) {
        dispatch({
          type: LE,
          payload: respuesta.data
        });
        
        getUserAuth();
      } else {
        console.log("Respuesta exitosa no esperada");
      }
      
    } catch (error) {
      let alerta;
      if (Array.isArray(error.response.data)) {
        alerta = {
          msg: 'Los campos no son validos',
          categoria: 'error'
        }
      } else {
        alerta = {
          msg: error.response.data.msg,
          categoria: 'error'
        }
      }
      dispatch({
        type: LF,
        payload: alerta
      });
    }
  }

  const logOut = () => {
    dispatch({
      type: CS
    });
  }



  return (
    <Context.Provider
      value={
        {
          token,
          autenticado,
          usuario,
          mensaje,
          cargando,

          addUser,
          logIn,
          getUserAuth,
          logOut
        }
      }
    >
      { props.children }
    </Context.Provider>
  );
}

export default State;