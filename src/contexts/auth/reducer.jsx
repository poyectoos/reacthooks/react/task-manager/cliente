import { RE, RF, OU, LE, LF, CS } from '../../types';

const Reducer = (state, action) => {
  switch (action.type) {
    // Registro exitoso
    case RE: 
      localStorage.setItem('token', action.payload.token);
      return {
        ...state,
        autenticado: true,
        mensaje: null,
        cargando: false,
        token: action.payload.token
      }
    // Obtenemos data del usuario logueado
    case OU:
      return {
        ...state,
        autenticado: true,
        usuario: action.payload,
        cargando: false
      } 
    // Loig in exitoso
    case LE: 
      localStorage.setItem('token', action.payload.token);
      return {
        ...state,
        autenticado: true,
        mensaje: null,
        cargando: false,
        token: action.payload.token
      }
    case LF:
    case RF:
    case CS:
      localStorage.removeItem('token')
      return {
        ...state,
        token: null,
        autenticado: null,
        usuario: null,
        mensaje: action.payload ? action.payload : null 
      }
    default:
      return state;
  }
}

export default Reducer;