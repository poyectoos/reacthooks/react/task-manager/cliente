
import 
  {
    PCFM, PCFO, PLOP, PLAP,
    PLME, PAMS, PEPS, PAE
  } from '../../types/index';


const Reducer = (state, action) => {
  console.log();
  switch (action.type) {
    case PCFM:
      return {
          ...state,
          visible: true
      }
    case PCFO:
      return {
          ...state,
          visible: false,
          error: false
      }
    case PLOP:
      return {
          ...state,
          proyectos: action.payload
      }
    case PLAP:
      return {
          ...state,
          proyectos: [
            action.payload,
            ...state.proyectos
          ],
          visible: false,
          error: false
      }
    case PLME:
      return {
        ...state,
        error: true
      }
    case PAMS:
      return {
        ...state,
        visible: false,
        error: false,
        proyecto: state.proyectos.filter(
          proyecto => proyecto.id === action.payload
        )[0]
      }
    case PEPS:
      return {
        ...state,
        visible: false,
        error: false,
        proyectos: state.proyectos.filter(
          proyecto => proyecto.id !== action.payload
        ),
        proyecto: null
      }
    case PAE: 
      return {
        ...state,
        mensaje: action.payload
      }
    default:
      return state;
  }
}

export default Reducer;