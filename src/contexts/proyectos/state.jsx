import React, { useReducer } from 'react';

import {
  PCFM, PCFO, PLOP, PLAP,
  PLME, PAMS, PEPS, PAE
} from '../../types';

import Context from './context';
import Reducer from './reducer';

import ClienteAxios from '../../config/axios';

const State = props => {

  // Se inicializa el estado
  const initialState = {
    // True: el formulario para agregar proyecto es visible
    // False: el formulario no es visible
    visible: false,
    proyectos: [],
    error: false,
    proyecto: null,
    mensaje: null
  }


  // Dispatch para ejecutar las acciones
  const [ state, dispatch ] = useReducer(Reducer, initialState);

  const { visible, proyectos, error, proyecto, mensaje } = state;

  // Serie de funcione para el CURD
  const handleShow = () => {
    dispatch({
      type: PCFM
    });
  }
  const handleHide = () => {
    dispatch({
      type: PCFO
    });
  }

  // Se obtienen los proyectos
  const obtenerProyectos = async proyectos => {
    try {
      const respuesta = await ClienteAxios.get('/api/proyectos');

      if (respuesta.status === 200) {
        dispatch({
          type: PLOP,
          payload: respuesta.data.map(
            proyecto => {
              return {
                id: proyecto._id,
                nombre: proyecto.nombre,
                create_at: proyecto.create_at
              }
            }
          )
        });
      } else {
        console.log("Respuesta exitosa no esperada");
      }
    } catch (error) {
      const alerta = {
        msg: 'Error al obtener el proyecto',
        categoria: 'error'
      }
      dispatch({
        type: PAE,
        payload: alerta
      });
    }
  }

  // Agregar nuevo proyecto
  const agregarProyecto = async proyecto => {
    try {
      const { nombre } = proyecto;
      const respuesta = await ClienteAxios.post('/api/proyectos', { nombre });

      if (respuesta.status === 200) {
        const { id, nombre, create_at } = respuesta.data;
        console.log();
        dispatch({
          type: PLAP,
          payload: {
            id,
            nombre,
            create_at
          }
        });
        const alerta = {
          msg: 'Proyecto agregado correctamente',
          categoria: 'ok'
        }
        dispatch({
          type: PAE,
          payload: alerta
        });
        // await handleSelectProject(id);
      } else {
        console.log("Respuesta exitosa no esperada");
      }
    } catch (error) {
      const alerta = {
        msg: 'Error al agregar el proyecto',
        categoria: 'error'
      }
      dispatch({
        type: PAE,
        payload: alerta
      });
    }
    
  }

  // Mostrar error de formulario
  const handleShowError = () => {
    dispatch({
      type: PLME,
    });
  }

  // Selecciona el proyecto que el usuario selecciono
  const handleSelectProject = async id => {
    dispatch({
      type: PAMS,
      payload: id
    });
  }

  // Eliminar proyecto seleccionado
  const handleDeleteProject = async id => {
    try {
      const respuesta = await ClienteAxios.delete(`/api/proyectos/proyecto/${id}`);
      if (respuesta.status === 200) {
        dispatch({
          type: PEPS,
          payload: id
        })
        
        const alerta = {
          msg: 'Proyecto eliminado correctamente',
          categoria: 'ok'
        }
        dispatch({
          type: PAE,
          payload: alerta
        });
      } else {
        console.log("Respuesta exitosa no esperada");
      }
    } catch (error) {
      const alerta = {
        msg: 'Error al eliminar el proyecto',
        categoria: 'error'
      }
      dispatch({
        type: PAE,
        payload: alerta
      });
    }
  }

  return (
    <Context.Provider
      value={
        {
          visible,
          proyectos,
          error,
          proyecto,
          mensaje,
          
          handleShow,
          handleHide,
          obtenerProyectos,
          agregarProyecto,
          handleShowError,
          handleSelectProject,
          handleDeleteProject,
        }
      }
    >
      { props.children }
    </Context.Provider>
  );
}

export default State;