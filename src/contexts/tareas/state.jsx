import React, { useReducer } from 'react';

import Context from './context';
import Reducer from './reducer';

import ClienteAxios from '../../config/axios';

import 
  {
    TLOT, TPAT, TPME, TPET, TLST, TPEP, TLDT, TAE
  } from '../../types';

const State = props => {
  const initial = {
    seleccion: [],
    error: false,
    tarea: null,
    mensaje: null
  }

  // Creamos el dispath y state
  const [ state, dispatch ] = useReducer(Reducer, initial);

  const { seleccion, error, tarea, mensaje } = state;

  // Obtener tareas de un proyecto
  const handleGetTareas = async id => {
    // Id corresppnde al ide del proyecto
    try {
      const respuesta = await ClienteAxios.get('/api/tareas', { params: { proyecto: id } });
      if (respuesta.status === 200) {
        dispatch({
          type: TLOT,
          payload: respuesta.data.map(
            tarea => {
              return {
                create_at: tarea.create_at,
                estado: tarea.estado,
                nombre: tarea.nombre,
                proyecto: tarea.proyecto,
                id: tarea._id,
              }
            }
          )
        });
      } else {
        console.log("Respuesta exitosa no esperada");
      }
    } catch (error) {
      const alerta = {
        msg: 'Error al obtener el listado de tareas',
        categoria: 'error'
      }
      dispatch({
        type: TAE,
        payload: alerta
      });
    }
  }

  // Agregar tarea a proyecto seleccionado
  const handleAddTarea = async tarea => {
    try {
      const { nombre, proyecto } = tarea;
      const respuesta = await ClienteAxios.post('/api/tareas', {nombre, proyecto});
      if (respuesta.status === 200) {
        const { id, nombre, estado, proyecto, create_at } = respuesta.data;
        dispatch({
          type: TPAT,
          payload: {
            id,
            nombre,
            estado,
            proyecto,
            create_at,
          }
        });
        const alerta = {
          msg: 'Tarea agregada correctaente',
          categoria: 'ok'
        }
        dispatch({
          type: TAE,
          payload: alerta
        });
      } else {
        console.log("Respuesta de exito no esperada");
      }
    } catch (error) {
      const alerta = {
        msg: 'Error al agregar la tarea',
        categoria: 'error'
      }
      dispatch({
        type: TAE,
        payload: alerta
      });
    }
  }

  // Mostrr error en formulario para agregar tarea
  const handleShowErrorForm = () => {
    dispatch({
      type: TPME
    });
  }

  // Eliinar tarea
  const handleDeleteTarea = async (id) => {

    try {
      const respuesta = await ClienteAxios.delete(`/api/tareas/tarea/${id}`);
      if (respuesta.status === 200) {
        dispatch({
          type: TPET,
          payload: id
        });
        const alerta = {
          msg: 'Tarea eliminada correctaente',
          categoria: 'ok'
        }
        dispatch({
          type: TAE,
          payload: alerta
        });
      } else {
        console.log("Respuesta exitosa no esperada");
      }

    } catch (error) {
      const alerta = {
        msg: 'Error al eliminar la tarea',
        categoria: 'error'
      }
      dispatch({
        type: TAE,
        payload: alerta
      });
    }
  }

  // Seleccionar tarea que se va a editar alv
  const handleSelectTarea = id => {
    dispatch({
      type: TLST,
      payload: id
    });
  }

  // Editar tarea
  const handleEditTarea = async tarea => {
    try {
      const { estado, id, nombre } = tarea;
      const respuesta = await ClienteAxios.put(`/api/tareas/tarea/${id}`, { estado, id, nombre });
      if (respuesta.status === 200) {
        dispatch({
          type: TPEP,
          payload: {
            create_at: respuesta.data.create_at,
            estado: respuesta.data.estado,
            id: respuesta.data.id,
            nombre: respuesta.data.nombre,
            proyecto: respuesta.data.proyecto,
          }
        });
        const alerta = {
          msg: 'Tarea editada correctaente',
          categoria: 'ok'
        }
        dispatch({
          type: TAE,
          payload: alerta
        });
      } else {
        console.log("Respuesta exitosa no esperada");
      }
    } catch (error) {
      const alerta = {
        msg: 'Error al modificar la tarea',
        categoria: 'error'
      }
      dispatch({
        type: TAE,
        payload: alerta
      });
    }
  }

  // Elimina la tarea seleccionada para la edicion
  const handleUnselectTarea = () => {
    dispatch({
      type: TLDT
    });
  }
  
  return (
    <Context.Provider
      value={
        {
          seleccion,
          error,
          seleccionada: tarea,
          mensaje,

          handleGetTareas,
          handleAddTarea,
          handleShowErrorForm,
          handleDeleteTarea,
          handleSelectTarea,
          handleEditTarea,
          handleUnselectTarea
        }
      }
    >
      { props.children }
    </Context.Provider>
  );
}

export default State;