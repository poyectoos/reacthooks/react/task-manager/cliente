
import 
  {
    TLOT, TPAT, TPME, TPET,
    TLST, TPEP, TLDT, TAE
  } from '../../types';

const Reducer = (state, action) => {
  switch (action.type) {
    case TLOT:
      return {
        ...state,
        seleccion: action.payload
      }
    case TPAT:
      return {
        ...state,
        seleccion: [
          action.payload,
          ...state.seleccion
        ],
        error: false
      }
    case TPME: 
      return {
        ...state,
        error: true
      }
    case TPET:
      return {
        ...state,
        seleccion: state.seleccion.filter(
          tarea => tarea.id !== action.payload
        )
      }
    case TPEP:
      return {
        ...state,
        seleccion: state.seleccion.map(
          tarea => 
            tarea.id === action.payload.id
              ?
            action.payload
              :
            tarea
        )
      }
    case TLST:
      return {
        ...state,
        tarea: state.seleccion.filter(
          tarea => tarea.id === action.payload
        )[0]
      }
    case TLDT:
      return {
        ...state,
        tarea: null,
        error: false
      }
    case TAE: 
        return {
          ...state,
          mensaje: action.payload
        }
    default:
      return state;
  }
}



export default Reducer;