import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import LogIn from './components/auth/LogIn';
import CrearCuenta from './components/auth/CrearCuenta';
import Proyectos from './components/proyectos/Proyectos';

import Privada from './components/rutas/Privada';

import ProyectoState from './contexts/proyectos/state';
import AlertaState from './contexts/alerta/state';
import AuthState from './contexts/auth/state';

import AuthToken from './config/token';

const token = localStorage.getItem('token');
  
if (token) {
  AuthToken(token);
} 

function App() {
  return (
    <AlertaState>
      <AuthState>
        <Router>
          <Switch>
            <Route
              exact
              path="/"
              component={LogIn}
            />
            <Route
              exact
              path="/cuenta/crear"
              component={CrearCuenta}
            />
            <ProyectoState>
              <Privada
                exact
                path="/proyectos"
                component={Proyectos}
              />
            </ProyectoState>
          </Switch>
        </Router>
      </AuthState>
    </AlertaState>
  );
}

export default App;
