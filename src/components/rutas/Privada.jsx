import React, { useContext, useEffect } from 'react';
import { Route, Redirect } from 'react-router-dom';

import AuthContext from '../../contexts/auth/context';

const Privada = ({ component: Component, ...props }) => {

  const { autenticado, getUserAuth, cargando } = useContext(AuthContext);

  useEffect(() => {
    getUserAuth();
    // eslint-disable-next-line
  }, []);

  return (
    <Route
      { ...props }
      render={
        props =>
        !autenticado && !cargando
          ?
        <Redirect to="/" />
          :
        <Component { ...props } />
      }
    />
  );
};

export default Privada;