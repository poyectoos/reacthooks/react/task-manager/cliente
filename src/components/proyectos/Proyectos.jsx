import React, { Fragment, useContext, useEffect } from 'react';

import Sidebar from '../layout/Sidebar';
import Navbar from '../layout/Navbar';

import Encabezado from '../tareas/Encabezado';
import Crear from '../tareas/Crear';
import Listado from '../tareas/Listado';

import TareasState from '../../contexts/tareas/state';

import ProyectoContext from '../../contexts/proyectos/context';
import AuthContext from '../../contexts/auth/context';
import AlertaContext from '../../contexts/alerta/context';


const Proyectos = () => {

  const { proyecto, mensaje } = useContext(ProyectoContext);

  const { alerta, handleShowAlert } = useContext(AlertaContext);



  const { getUserAuth } = useContext(AuthContext);

  useEffect(() => {
    getUserAuth();
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (mensaje) {
      handleShowAlert(mensaje.msg, mensaje.categoria)
    }
    // eslint-disable-next-line
  }, [mensaje]);

  return (
    <TareasState>
      {
        alerta
          ?
        <div className={`alerta alerta-${alerta.categoria} `}>
          { alerta.mensaje }
        </div>
          :
        null
      }
      <div className="contenedor-app">
        <Sidebar />
          <div className="seccion-principal">
            <Navbar />
            <main>
              {
                proyecto
                  ?
                <Fragment>
                  <Encabezado />
                  
                  <Crear />
                  <div className="contenedor-tareas">
                    <Listado />
                  </div>
                </Fragment>
                  :
                <h2 className="no-project">Selecciona un proyecto</h2>
              }
            </main>
          </div>
      </div>
    </TareasState>
  );
};

export default Proyectos;