import React, { Fragment, useState, useContext } from 'react';

import ProyectoContext from '../../contexts/proyectos/context';

const Crear = () => {

  const 
    {
      visible,
      error,

      handleShow,
      handleHide,
      agregarProyecto,
      handleShowError
    } = useContext(ProyectoContext);

  // State
  const [ proyecto, actualizarProyecto ] = useState({
    nombre: ''
  });

  const { nombre } = proyecto;

  const handleChange = e => {
    actualizarProyecto({
      ...proyecto,
      [e.target.name] : e.target.value
    })
  }

  const handleSubmit = e => {
    e.preventDefault();

    // Validar el pryecto asi bien chido
    if (nombre.trim() === '') {
      handleShowError();
      return;
    }
    
    // Se agrega al state
    agregarProyecto(proyecto);

    actualizarProyecto({
      nombre: ''
    });
  }

  const handleShowForm = async () => {
    await handleShow();
    document.querySelector('#nombre').focus();
  }
  const handleHideForm = () => {
    handleHide();
    actualizarProyecto({
      nombre: ''
    });
  }

  return (
    <Fragment>
      {
        visible
          ?
        <form
          className="formulario-nuevo-proyecto"
          onSubmit={ handleSubmit }
        >
          {
            error 
              ?
            <p className="alert alert-outline-danger">
              Proporciona un nombre para el proyecto
            </p>
              :
            null
          }
          <input
            type="text"
            className="input-text"
            name="nombre"
            id="nombre"
            placeholder="Nombre del proyecto"
            value={ nombre }
            onChange={ handleChange }
          />
          <button
            type="submit"
            className="btn btn-success btn-block"
          >
            Agregar
          </button>
          <button
            type="submit"
            className="btn btn-danger btn-block"
            onClick={ handleHideForm }
          >
            Cancelar
          </button>
        </form>
          :
        <button
          type="button"
          className="btn btn-primario btn-block"
          onClick={ handleShowForm }
        >
          Nuevo proyecto
        </button>
      }
    </Fragment>
  );
};

export default Crear;