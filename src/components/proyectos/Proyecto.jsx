import React, { useContext } from 'react';
import PropTypes from 'prop-types';

import ProyectoContext from '../../contexts/proyectos/context';
import TareasContext from '../../contexts/tareas/context';

const Proyecto = ({ proyecto }) => {

  const { id, nombre } = proyecto;

  const { handleSelectProject } = useContext(ProyectoContext);

  const { handleGetTareas, handleUnselectTarea } = useContext(TareasContext);

  const handleSelect = () => {
    handleSelectProject(id);
    handleGetTareas(id);
    handleUnselectTarea();
  }

  return (
    <li>
      <button
        type="button"
        className="btn btn-blank"
        onClick={ handleSelect }
      >
        { nombre }
      </button>
    </li>
  );
};

Proyecto.propTypes = {
  proyecto: PropTypes.object.isRequired
};


export default Proyecto;