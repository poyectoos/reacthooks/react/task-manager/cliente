import React, { useContext, useEffect } from 'react';

import Proyecto from './Proyecto';

import ProyectoContext from '../../contexts/proyectos/context';

const Listado = () => {

  const { proyectos, obtenerProyectos } = useContext(ProyectoContext);


  useEffect(() => {
    obtenerProyectos([]);
    // eslint-disable-next-line
  }, []);

  return (
    <ul className="listado-proyectos">
      {
        proyectos.length === 0
          ?
        <li className="text-center">
          <p>No hay proyectos</p>
        </li>
          :
        proyectos.map(proyecto => (
          <Proyecto
            key={ proyecto.id }
            proyecto={ proyecto }
          />
        ))
      }
    </ul>
  );
};

export default Listado;