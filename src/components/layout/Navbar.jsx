import React, { useContext, useEffect } from 'react';

import AuthContext from '../../contexts/auth/context';

const Navbar = () => {
  
  const { getUserAuth, usuario, logOut } = useContext(AuthContext);

  useEffect(() => {
    getUserAuth();
    // eslint-disable-next-line
  }, []);

  const handleLogOut = () => {
    logOut();
  }


  return (
    <header className="app-header">
      <p className="nombre-usuario">Hola <span>{ usuario ? usuario.nombre : null }</span> </p>

      <nav className="nav-principal">
        <button
          type="button"
          className="btn text-white btn-blank cerrar-sesion padding_cerrer-sesion"
          onClick={ handleLogOut }
        >
          Cerrar sesión
        </button>
      </nav>
    </header>
  );
};

export default Navbar;