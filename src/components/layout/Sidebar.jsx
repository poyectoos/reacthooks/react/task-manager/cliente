import React from 'react';

import Crear from '../proyectos/Crear';
import Listado from '../proyectos/Listado';

import Logo from '../../Logo.png';

const Sidebar = () => {
  return (
    <aside className="sidebar">
      <div className="header text-center">
        <img src={ Logo } alt="Logo" width="65" height="65" />
        <h1>Darth Malak</h1>
        <h3>Project Manager</h3>
      </div>

      <div>
        <Crear />
      </div>

      <div className="proyectos">
        <h2>Tus proyectos</h2>
        <Listado />
      </div>
    </aside>
  );
};

export default Sidebar;