import React, { useState, useContext,useEffect } from 'react';
import { Link } from 'react-router-dom';

import AlertaContext from '../../contexts/alerta/context';
import AuthContext from '../../contexts/auth/context';

const CrearCuenta = ({ history }) => {

  // Context de alerta
  const { alerta, handleShowAlert } = useContext(AlertaContext);

  // COntext de auth
  const { mensaje, autenticado, addUser } = useContext(AuthContext);

  const [ credenciales, actualizarCredenciales ] = useState({
    nombre: '',
    email: '',
    password: '',
    confirmPassword: ''
  });

  const { nombre, email, password, confirmPassword } = credenciales;

  useEffect(() => {
    if (autenticado) {
      history.push('/proyectos')
    }
    if (mensaje) {
      handleShowAlert(mensaje.msg, mensaje.categoria);
    }
  }, [mensaje, autenticado, history, handleShowAlert]);


  const handleChange = e => {
    actualizarCredenciales({
      ...credenciales,
      [e.target.name] : e.target.value
    });
  }
  const handleSubmit = e => {
    e.preventDefault();
    if ( nombre.trim() === '' || email.trim() === '' || password.trim() === '' || confirmPassword.trim() === '' ) {
      handleShowAlert('Todos los campos son obligatorios', 'error');
      return;
    }

    if (password.length < 8) {
      handleShowAlert('La contraseña debe tener al menos 8 caracteres', 'error');
      return;
    }

    if (password !== confirmPassword) {
      handleShowAlert('Ambas contraseñas deben ser iguales', 'error');
      return;
    }

    addUser({ nombre, email, password});
  }

  return (
    <div className="form-usuario">
      {
        alerta
          ?
        <div className={`alerta alerta-${alerta.categoria} `}>
          { alerta.mensaje }
        </div>
          :
        null
      }
      <div className="contenedor-form sombra-dark">
        <h1>Crear cuenta</h1>
        
        <form
          onSubmit={ handleSubmit }
        >
          <div className="campo-form">
            <label htmlFor="nombre">Nombre</label>
            <input
              type="text"
              id="nombre"
              name="nombre"
              value={ nombre }
              onChange={ handleChange }
            />
          </div>
          <div className="campo-form">
            <label htmlFor="email">Correo electrónico</label>
            <input
              type="email"
              id="email"
              name="email"
              value={ email }
              onChange={ handleChange }
            />
          </div>
          <div className="campo-form">
            <label htmlFor="password">Contraseña</label>
            <input
              type="password"
              id="password"
              name="password"
              value={ password }
              onChange={ handleChange }
            />
          </div>
          <div className="campo-form">
            <label htmlFor="confirmPassword">Confirmar contraseña</label>
            <input
              type="password"
              id="confirmPassword"
              name="confirmPassword"
              value={ confirmPassword }
              onChange={ handleChange }
            />
          </div>
          <div className="campo-form">
            <button
              type="submit"
              className="btn btn-primario btn-block"
            >
              Crear
            </button>
          </div>
          <Link
            to='/'
            className="enlace-cuenta text-center"
          >
            Ya tengo una cuenta
          </Link>
        </form>
      </div>
    </div>
  );
};

export default CrearCuenta;