import React, { Fragment, useState, useContext, useEffect } from 'react';
import { Link } from 'react-router-dom';

import AlertaContext from '../../contexts/alerta/context';
import AuthContext from '../../contexts/auth/context';

const LogIn = ({ history }) => {

  // Context de alerta
  const { alerta, handleShowAlert } = useContext(AlertaContext);

  // COntext de auth
  const { mensaje, autenticado, logIn } = useContext(AuthContext);

  const [ credenciales, actualizarCredenciales ] = useState({
    email: '',
    password: ''
  });

  const { email, password } = credenciales;

  
  useEffect(() => {
    if (autenticado) {
      history.push('/proyectos')
    }
    if (mensaje) {
      handleShowAlert(mensaje.msg, mensaje.categoria);
    }
  }, [mensaje, autenticado, history, handleShowAlert]);

  const handleChange = e => {
    actualizarCredenciales({
      ...credenciales,
      [e.target.name] : e.target.value
    });
  }
  const handleSubmit = e => {
    e.preventDefault();
    if (email.trim() === '' || password.trim() === '') {
      handleShowAlert('Todos los campos son obligatorios', 'error');
      return;
    }

    logIn({
      email: email.trim(),
      password: password.trim(),
    });
  }

  return (
    <Fragment>
      {
        alerta
          ?
        <div className={`alerta alerta-${alerta.categoria} `}>
          { alerta.mensaje }
        </div>
          :
        null
      }
      <div className="form-usuario">
        <div className="contenedor-form sombra-dark">
          <h1>Iniciar Sesión</h1>
          
          <form
            onSubmit={ handleSubmit }
          >
            <div className="campo-form">
              <label htmlFor="email">Correo electrónico</label>
              <input
                type="email"
                id="email"
                name="email"
                value={ email }
                onChange={ handleChange }
              />
            </div>
            <div className="campo-form">
              <label htmlFor="password">Contraseña</label>
              <input
                type="password"
                id="password"
                name="password"
                value={ password }
                onChange={ handleChange }
              />
            </div>
            <div className="campo-form">
              <button
                type="submit"
                className="btn btn-primario btn-block"
              >
                Iniciar
              </button>
            </div>
            <Link
              to='/cuenta/crear'
              className="enlace-cuenta text-center"
            >
              Crear cuenta
            </Link>
          </form>
        </div>
      </div>
    </Fragment>
  );
};

export default LogIn;