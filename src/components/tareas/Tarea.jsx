import React, { useContext } from 'react';
import PropTypes from 'prop-types';

import TareasContext from '../../contexts/tareas/context';

import ProyectoContext from '../../contexts/proyectos/context';

const Tarea = ({ tarea }) => {

  // COntext para tareas
  const 
    {
      seleccionada,

      handleDeleteTarea,
      handleGetTareas,
      handleEditTarea,
      handleSelectTarea
    } = useContext(TareasContext);

  // Context Proyecto
  const { proyecto } = useContext(ProyectoContext);

  const { id, nombre, estado } = tarea;

  const handleDelete = () => {
    handleDeleteTarea(id);
    handleGetTareas(proyecto.id);
  }
  const handleChange = () => {
    handleEditTarea({
      ...tarea,
      estado: !estado
    });
    handleGetTareas(proyecto.id);
  }
  const handleSelect = () => {
    handleSelectTarea(id);
  }

  return (
    <li className="tarea sombra">
      <p>{ nombre }</p>
      <div className="estado">

        {
          estado
            ?
          <button
            type="button"
            className="completo"
            onClick={ handleChange }
            disabled={ seleccionada !== null }
          >
            Completo
          </button>
            :
          <button
            type="button"
            className="incompleto"
            onClick={ handleChange }
            disabled={ seleccionada !== null }
          >
            Incompleto
          </button>
        }
      </div>
      <div className="acciones">
        <button
          type="button"
          className="btn btn-info"
          onClick={ handleSelect }
          disabled={ seleccionada !== null }
        >
          Editar
        </button>
        <button
          type="button"
          className="btn btn-danger"
          onClick={ handleDelete }
          disabled={ seleccionada !== null }
        >
          Eliminar
        </button>
      </div>
    </li>
  );
};

Tarea.propTypes = {
  tarea: PropTypes.object.isRequired
};

export default Tarea;