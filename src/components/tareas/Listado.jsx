import React, { Fragment, useContext, useEffect } from 'react';

import ProyectoContext from "../../contexts/proyectos/context";
import TareasContext from "../../contexts/tareas/context";
import AlertaContext from '../../contexts/alerta/context';

import Tarea from './Tarea';

const Listado = () => {

  const { alerta, handleShowAlert } = useContext(AlertaContext);

  // Context de tareas
  const { proyecto, handleDeleteProject } = useContext(ProyectoContext);

  // Context de tareas
  const { seleccion, seleccionada, mensaje } = useContext(TareasContext);

  useEffect(() => {
    if (mensaje) {
      handleShowAlert(mensaje.msg, mensaje.categoria)
    }
    // eslint-disable-next-line
  }, [mensaje]);

  if (!proyecto) return <h2>Selecciona un proyecto</h2>

  const handleDelete = () => {
    handleDeleteProject(proyecto.id);
  }

  return (
    <Fragment>
      {
        alerta
          ?
        <div className={`alerta alerta-${alerta.categoria} `}>
          { alerta.mensaje }
        </div>
          :
        null
      }
      <ul className="listado-tareas">
        {
          seleccion.length === 0
            ?
          <li>
            <h3 className="text-center text-gray-2 mb-3">
              No hay tareas para mostrar en este proyecto
            </h3>
          </li>
            :
          seleccion.map(tarea => (
            <Tarea
              key={ tarea.id }
              tarea={ tarea }
            />
          ))
        }
      </ul>
      
      <div className="text-center">
        <button
          type="button"
          className="btn btn-danger"
          onClick={ handleDelete }
          disabled={ seleccionada !== null }
        >
          Eliminar proyecto
        </button>
      </div>
    </Fragment>
  );
};

export default Listado;