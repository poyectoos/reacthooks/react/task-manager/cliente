import React, { useState, useContext, useEffect } from 'react';

import ProyectoContext from "../../contexts/proyectos/context";

import TareasContext from "../../contexts/tareas/context";


const Crear = () => {

  // Coontext de proyectos
  const { proyecto } = useContext(ProyectoContext);
  
  //Context de tareas
  const 
    {
      error,
      seleccionada,
      
      handleAddTarea,
      handleShowErrorForm,
      handleGetTareas,
      handleEditTarea,
      handleUnselectTarea
    
    } = useContext(TareasContext);

  // State del formulario
  const [ tarea, actualizarTarea ] = useState({
    nombre: ''
  });

  const { nombre } = tarea;

  // Limpiar la tarea cuando se cambie de proyecto
  useEffect(() => {
    handleResetForm();
    document.querySelector('#nombre').focus();
  }, [proyecto]);

  // Detectamos si la tarea cambio
  useEffect(() => {
    if (!seleccionada) return;
    actualizarTarea(seleccionada);
  }, [seleccionada]);

  const handleSubmit = e => {
    e.preventDefault();
    const { id } = proyecto;

    if (nombre.trim() === '') {
      handleShowErrorForm();
      return;
    }

    if (!seleccionada) {
      handleAddTarea({
        ...tarea,
        proyecto: id,
      });
    } else {
      handleEditTarea(tarea);
      handleUnselectTarea();
    }

    
    handleResetForm();
    handleGetTareas(id);
  }

  const handleChange = e => {
    actualizarTarea({
      ...tarea,
      [e.target.name] : e.target.value
    });
  }

  const handleResetForm = () => {
    actualizarTarea({
      nombre: ''
    });
  }

  const handleCancel = () => {
    handleResetForm();
    handleUnselectTarea();
  }

  return (
    <div className="formulario">
      {
        error
          ?
        <div className="alert alert-danger mb-1">
          Proporciona un titulo para la tarea
        </div>
          :
        null
      }
      <form
        onSubmit={ handleSubmit }
      >
        <div className="contenedor-input">
          <input
            type="text"
            className="input-text"
            placeholder="Titulo de la tarea"
            name="nombre"
            id="nombre"
            value={ nombre }
            onChange={ handleChange }
          />
        </div>
        <div className="contenedor-input">
          <button
            type="submit"
            className="btn btn-primario btn-success btn-block"
          >
            {
              !seleccionada
                ?
              'Agregar'
                :
              'Editar'
            }
          </button>
          {
            seleccionada
              ?
            <button
              type="button"
              className="btn btn-danger btn-block ml-1"
              onClick={ handleCancel }
            >
              Cancelar
            </button>
              :
            null
          }
        </div>
      </form>
    </div>
  );
};

export default Crear;