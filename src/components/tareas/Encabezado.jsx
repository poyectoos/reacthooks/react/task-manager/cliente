import React, { useContext } from 'react';

import ProyectoContext from "../../contexts/proyectos/context";

const Encabezado = () => {

  const { proyecto } = useContext(ProyectoContext);

  return (
    <div className="header tareas text-white">
      <h2>Proyecto: { proyecto.nombre }</h2>
    </div>
  );
};

export default Encabezado;