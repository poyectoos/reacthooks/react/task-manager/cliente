import AuthToken from '../config/token';

export const haveToken = () => {
  const token = localStorage.getItem('token');
  
  if (token) {
    return AuthToken(token);
  } else {
    return null;
  }
}
