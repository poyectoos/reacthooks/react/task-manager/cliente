export const PCFM = 'proyecto_crear_formulario_mostrar';
export const PCFO = 'proyecto_crear_formulario_ocultar';
export const PLOP = 'proyecto_listado_obtener_proyectos';
export const PLAP = 'proyecto_listado_agregar_proyecto';
export const PLME = 'proyecto_listado_mostrar_error';
export const PAMS = 'proyecto_actual_mostrar_seleccion';
export const PEPS = 'proyecto_eliminar_proyecto_seleccion';
export const PAE = 'proyecto_alerta_error';


export const TLOT = 'tareas_listado_obtener_tareas';
export const TPAT = 'tarea_proyecto_agregar_tarea';
export const TPME = 'tarea_proyecto_mostrar_error';
export const TPET = 'tarea_proyecto_eliminar_tarea';
export const TLCE = 'tarea_listado_cambiar_estado';
export const TLST = 'tarea_listado_seleccionar_tarea';
export const TPEP = 'tarea_proyecto_editar_tarea';
export const TLDT = 'tarea_listado_deseleccionar_tarea';
export const TAE = 'proyecto_alerta_error';

export const MA = 'mostrar_alerta';
export const OA = 'ocultar_alerta';

export const RE = 'registro_exitoso';
export const RF = 'registro_no_exitoso';
export const OU = 'obtener_usuario';
export const LE = 'login_exitoso';
export const LF = 'login_no_exitoso';
export const CS = 'cerrar_sesion';
